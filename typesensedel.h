#ifndef TYPESENSEDEL_H
#define TYPESENSEDEL_H

#include <lib/net/httplib/http.h>
#include <lib/net/httpaccessmanager.h>
#include <lib/3rdparty/json.hpp>

class TypeSenseDel
{
public:
    TypeSenseDel();
    int deleteCollection();
    void setDontDeletedCollections();

private:
    Http *http;
    std::vector<std::string> collections;

    std::string collectionParser(std::string s);
    std::vector<std::string> findCollectionNames();
};

#endif // TYPESENSEDEL_H
