#include "typesensedel.h"

TypeSenseDel::TypeSenseDel()
{
    http = new Http("10.5.184.156:8108");
    http->setContentType("application/json");
    http->setHeader("X-TYPESENSE-API-KEY", "sparse");
}

int TypeSenseDel::deleteCollection(){
    bool checkCollectionName = false;
    std::vector<std::string> collectionNames = findCollectionNames();
    for (int i = 0; i < collectionNames.size(); ++i){
        for (int j = 0; j < collections.size(); ++j)
            if(collections[j] == collectionNames[i])
            {
                std::cout << collectionNames[i] << " Silinmedi." << std::endl;
                checkCollectionName = true;
                break;
            }
        if (!checkCollectionName){
            http->setPath("/collections/" + collectionNames[i]);
            int ret = http->del();
            if(ret)
                return ret;
            std::cout << collectionNames[i] << " Silindi" << std::endl;
        }
        checkCollectionName = false;
    }
    return 0;
}

void TypeSenseDel::setDontDeletedCollections(){
    Http *http_1;
    http_1 = new Http("10.5.184.166:30555");
    http_1->setContentType("application/json");
    http_1->setHeader("X-TYPESENSE-API-KEY", "sparse");
    http_1->setPath("/v1/kv/nvr?recurse=true&keys=true");
    http_1->get();
    std::string c_name = http->getBody();

    std::string delimiter = ",";
    size_t pos = 0;
    std::vector<std::string> token;
    while ((pos = c_name.find(delimiter)) != std::string::npos) {
        token.push_back(c_name.substr(0, pos));
        c_name.erase(0, pos + delimiter.length());
    }
    for (int i = 1; i < token.size(); i = i + 5) {
        collections.push_back(collectionParser(token[i]));
    }
}

std::string TypeSenseDel::collectionParser(std::string s){
    std::string delimiter = "/";
    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        s.erase(0, pos + delimiter.length());
    }
    s.pop_back();
    return s;
}

std::vector<std::string> TypeSenseDel::findCollectionNames(){
    std::vector<std::string> collectionNames;
    http->setPath("/collections");
    http->get();
    nlohmann::json js = nlohmann::json::parse(http->getBody());
    for (int i = 0; i < js.size(); ++i) {
        collectionNames.push_back(js[i]["name"]);
    }

    return collectionNames;
}
